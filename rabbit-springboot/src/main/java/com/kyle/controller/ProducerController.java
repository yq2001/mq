package com.kyle.controller;

import com.kyle.constants.Constants;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author kyle.yan
 * @since 2024-09-20
 * Description:
 */
@Slf4j
@RestController
@RequestMapping("/producer")
public class ProducerController {

    @Resource
    private RabbitTemplate rabbitTemplate;

    /**
     * 简单工作-队列模式
     */
    @RequestMapping("/work")
    public String work() {
        for (int i = 1; i <= 10; i++) {
            rabbitTemplate.convertAndSend("", Constants.WORK_QUEUE, "hello,springBoot started!" + i);
        }
        return "发送成功";
    }

    /**
     * 发布订阅模式
     */
    @RequestMapping("/fanout")
    public String fanout() {
        rabbitTemplate.convertAndSend(Constants.FANOUT_EXCHANGE, "", "hello,发布订阅模式发送的消息");
        log.info("发送成功");
        return "发送成功";
    }

    /**
     * 路由模式
     */
    @RequestMapping("/direct/{routingKey}")
    public String direct(@PathVariable("routingKey") String routingKey) {
        rabbitTemplate.convertAndSend(Constants.DIRECT_EXCHANGE, routingKey, "hello,路由模式发送的消息，my routing key is " + routingKey);
        log.info("发送成功");
        return "发送成功";
    }

    /**
     * topic模式
     */
    @RequestMapping("/topic/{routingKey}")
    public String topic(@PathVariable("routingKey") String routingKey) {
        rabbitTemplate.convertAndSend(Constants.TOPIC_EXCHANGE, routingKey, "hello,topic模式发送的消息，my routing key is " + routingKey);
        log.info("发送成功");
        return "发送成功";
    }

}
