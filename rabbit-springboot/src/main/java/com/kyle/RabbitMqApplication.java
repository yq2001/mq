package com.kyle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author kyle.yan
 * @since 2024-09-20
 * Description:
 */
@SpringBootApplication
//@ComponentScan("com.kyle.*")
public class RabbitMqApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitMqApplication.class, args);
    }

}
