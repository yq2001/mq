package com.kyle.direct;

import com.kyle.constants.Connect;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: m'y'q
 * \* Date: 2024/9/17 14:02
 * \* Description:
 * \
 */
public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(Connect.HOST);
        connectionFactory.setPort(Connect.PORT);
        connectionFactory.setUsername(Connect.USER_NAME);
        connectionFactory.setPassword(Connect.PASSWORD);
        connectionFactory.setVirtualHost(Connect.VIRTUAL_HOST);
        Connection connection = connectionFactory.newConnection();

        Channel channel = connection.createChannel();

        channel.exchangeDeclare(Connect.DIRECT_EXCHANGE, BuiltinExchangeType.DIRECT, true);

        channel.queueDeclare(Connect.DIRECT_QUEUE1, true, false, false, null);
        channel.queueDeclare(Connect.DIRECT_QUEUE2, true, false, false, null);
        // 绑定队列与交换机
        channel.queueBind(Connect.DIRECT_QUEUE1, Connect.DIRECT_EXCHANGE, "a");
        channel.queueBind(Connect.DIRECT_QUEUE2, Connect.DIRECT_EXCHANGE, "a");
        channel.queueBind(Connect.DIRECT_QUEUE2, Connect.DIRECT_EXCHANGE, "b");
        channel.queueBind(Connect.DIRECT_QUEUE2, Connect.DIRECT_EXCHANGE, "c");

        String msg_a = "hello direct, my routingKey is a....";
        channel.basicPublish(Connect.DIRECT_EXCHANGE, "a", null, msg_a.getBytes());

        String msg_b = "hello direct, my routingKey is b....";
        channel.basicPublish(Connect.DIRECT_EXCHANGE, "b", null, msg_b.getBytes());

        String msg_c = "hello direct, my routingKey is c....";
        channel.basicPublish(Connect.DIRECT_EXCHANGE, "c", null, msg_c.getBytes());
        System.out.println("消息发送成功!");

        channel.close();
        connection.close();
    }
}
