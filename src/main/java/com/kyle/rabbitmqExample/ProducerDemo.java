package com.kyle.rabbitmqExample;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author kyle.yan
 * @since 2024-09-13
 * Description: 生产者实例代码
 */
public class ProducerDemo {
    public static void main(String[] args) throws IOException, TimeoutException {

        // 1. 建立连接
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("124.71.196.8");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("admin");
        connectionFactory.setPassword("admin");
        connectionFactory.setVirtualHost("doubleK");
        Connection connection = connectionFactory.newConnection();
        // 2. 开启信道
        Channel channel = connection.createChannel();
        System.out.println("已开启连接和信道!");
        // 3. 声明交换机 - 使用内置交换机
        // 4. 声明队列
        /**
         * queueDeclare(String queue, 队列名称
         *              boolean durable, 可持久化
         *              boolean exclusive, 是否独占
         *              boolean autoDelete, 是否自动删除
         *              Map<String, Object> arguments) 参数
         */
        channel.queueDeclare("Kplanet", true, false, false, null);
        System.out.println("已声明队列!");
        // 5. 发送消息
        /**
         * basicPublish(String exchange, 交换机名称
         *              String routingKey,  内置交换机和队列名称保持一致
         *              AMQP.BasicProperties props, 属性配置
         *              byte[] body) 消息内容
         */
        for (int i = 1; i <= 10; i++) {
            String msg = "i am working" + i;
            channel.basicPublish("", "Kplanet", null, msg.getBytes());
        }
        System.out.println("消息发送成功!");
        // 6. 释放资源
//        channel.close();
//        connection.close();

    }
}
