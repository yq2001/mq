package com.kyle.rabbitmqExample;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author kyle.yan
 * @since 2024-09-13
 * Description: 消费者示例代码
 */
public class ConsumerDemo {
    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {

        // 1. 建立链接
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("124.71.196.8");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("kyle");
        connectionFactory.setPassword("kyle");
        connectionFactory.setVirtualHost("doubleK");
        Connection connection = connectionFactory.newConnection();
        // 2. 创建信道
        Channel channel = connection.createChannel();
        // 3. 声明队列（生产者声明了就可以省略）
        channel.queueDeclare("Kplanet", true, false, false, null);
        // 4. 消费消息
        DefaultConsumer consumer = new DefaultConsumer(channel) {
            // 从队列中接收到消息就会执行方法
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                System.out.println("接收到消息: " + new String(body));
            }
        };
        /**
         * basicConsume(String queue, 队列名称
         *              boolean autoAck, 是否自动确认
         *              Consumer callback) 回调逻辑
         */
        channel.basicConsume("Kplanet", true, consumer);
        // 5. 等待完成释放资源
        Thread.sleep(3000);
//        channel.close();
//        connection.close();

    }
}
