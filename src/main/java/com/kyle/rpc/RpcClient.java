package com.kyle.rpc;

import com.kyle.constants.Connect;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: m'y'q
 * \* Date: 2024/9/18 19:34
 * \* Description:
 * \
 */
public class RpcClient {
    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        // 1. 建立连接、发送请求
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(Connect.HOST);
        connectionFactory.setPort(Connect.PORT);
        connectionFactory.setUsername(Connect.USER_NAME);
        connectionFactory.setPassword(Connect.PASSWORD);
        connectionFactory.setVirtualHost(Connect.VIRTUAL_HOST);
        Connection connection = connectionFactory.newConnection();
        // 开启信道
        Channel channel = connection.createChannel();
        // 使用默认交换机，声明队列
        channel.queueDeclare(Connect.RPC_REQUEST_QUEUE, true, false, false, null);
        channel.queueDeclare(Connect.RPC_RESPONSE_QUEUE, true, false, false, null);
        // 设置请求唯一标识
        String correlationID = UUID.randomUUID().toString();
        // 设置请求相关属性
        AMQP.BasicProperties properties = new AMQP.BasicProperties().builder()
                .correlationId(correlationID)
                .replyTo(Connect.RPC_REQUEST_QUEUE)
                .build();
        // 构建消息并发送
        String msg = "RPC模式客户端发送了一条消息";
        channel.basicPublish("", Connect.RPC_REQUEST_QUEUE, properties, msg.getBytes());

        // 接受响应
        // 使用阻塞队列来存储响应信息实现同步
        final BlockingQueue<String> responseQueue = new ArrayBlockingQueue<>(1);
        DefaultConsumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String resMsg = new String(body);
                System.out.println("接收到回调消息：" + resMsg);
                // 验证是否一致
                if (correlationID.equals(properties.getCorrelationId())) {
                    responseQueue.offer(resMsg);
                }
            }
        };
        channel.basicConsume(Connect.RPC_RESPONSE_QUEUE, true, consumer);
        String result = responseQueue.take();
        System.out.println("[RPC Client 响应结果]:"+ result);
    }
}
