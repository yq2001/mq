package com.kyle.rpc;

import com.kyle.constants.Connect;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: m'y'q
 * \* Date: 2024/9/18 19:34
 * \* Description:
 * \
 */
public class RpcServer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 1. 建立连接、接受请求
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(Connect.HOST);
        connectionFactory.setPort(Connect.PORT);
        connectionFactory.setUsername(Connect.USER_NAME);
        connectionFactory.setPassword(Connect.PASSWORD);
        connectionFactory.setVirtualHost(Connect.VIRTUAL_HOST);
        Connection connection = connectionFactory.newConnection();
        // 开启信道
        Channel channel = connection.createChannel();
        // 接受一条消息
        channel.basicQos(1);
        DefaultConsumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String request = new String(body);
                System.out.println("接收到消息：" + request);
                // 处理响应过程
                String response = "针对request:"+ request +", 响应成功";
                AMQP.BasicProperties basicProperties = new AMQP.BasicProperties().builder()
                        .correlationId(properties.getCorrelationId())
                        .build();
                channel.basicPublish("", Connect.RPC_RESPONSE_QUEUE, basicProperties, response.getBytes());
                // 手动确认
                channel.basicAck(envelope.getDeliveryTag(), false); // 要确认的消息是哪个， 是否批处理
            }
        };
        channel.basicConsume(Connect.RPC_REQUEST_QUEUE, false, consumer);
    }
}
