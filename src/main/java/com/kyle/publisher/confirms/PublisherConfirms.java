package com.kyle.publisher.confirms;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmListener;
import com.rabbitmq.client.Connection;
import com.kyle.constants.Connect;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.TimeoutException;

/**
 * @author kyle.yan
 * @since 2024-09-19
 * Description:
 */
public class PublisherConfirms {

    private static final Integer MESSAGE_COUNT = 1000;

    static Connection createConnection() throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(Connect.HOST);
        connectionFactory.setPort(Connect.PORT);
        connectionFactory.setUsername(Connect.USER_NAME);
        connectionFactory.setPassword(Connect.PASSWORD);
        connectionFactory.setVirtualHost(Connect.VIRTUAL_HOST);
        return connectionFactory.newConnection();
    }

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        //Strategy #1: Publishing Messages Individually
        //单独确认
//        publishingMessagesIndividually();

        //Strategy #2: Publishing Messages in Batches
        //批量确认
//        publishingMessagesInBatches();

        //Strategy #3: Handling Publisher Confirms Asynchronously
        //异步确认
        handlingPublisherConfirmsAsynchronously();

    }

    /**
     * 异步确认
     * 异步确认策略, 消息条数: 1000, 耗时: 53 ms
     */
    private static void handlingPublisherConfirmsAsynchronously() throws IOException, TimeoutException, InterruptedException {
        try (Connection connection = createConnection()) {
            // 1. 开启信道
            Channel channel = connection.createChannel();
            // 2. 开启信道为 confirm 模式
            channel.confirmSelect();
            // 3. 声明交换机、队列
            channel.queueDeclare(Connect.PUBLISHER_CONFIRMS_QUEUE3, true, false, false, null);
            // 4. 监听 confirm
            // 存储未确认的消息ID
            SortedSet<Long> confirmSeqNo = Collections.synchronizedSortedSet(new TreeSet<>());
            long start = System.currentTimeMillis();
            channel.addConfirmListener(new ConfirmListener() {
                @Override
                public void handleAck(long deliveryTag, boolean multiple) throws IOException {
                    if (multiple) {
                        confirmSeqNo.headSet(deliveryTag + 1).clear();
                    } else {
                        confirmSeqNo.remove(deliveryTag);
                    }
                }

                @Override
                public void handleNack(long deliveryTag, boolean multiple) throws IOException {
                    if (multiple) {
                        confirmSeqNo.headSet(deliveryTag + 1).clear();
                    } else {
                        confirmSeqNo.remove(deliveryTag);
                    }
                    //业务需要根据实际场景进行处理, 比如重发, 此处代码省略
                }
            });
            // 发送消息
            for (int i = 0; i < MESSAGE_COUNT; i++) {
                String msg = "hello publisher confirms" + i;
                long nextPublishSeqNo = channel.getNextPublishSeqNo();
                channel.basicPublish("", Connect.PUBLISHER_CONFIRMS_QUEUE3, null, msg.getBytes());
                confirmSeqNo.add(nextPublishSeqNo);
            }
            while (confirmSeqNo.isEmpty()) {
                Thread.sleep(10);
            }
            long end = System.currentTimeMillis();
            System.out.printf("异步确认策略, 消息条数: %d, 耗时: %d ms \n",MESSAGE_COUNT, end-start);
        }
    }

    /**
     * 批量确认
     * 批量确认策略, 消息条数: 1000, 耗时: 132 ms
     */
    private static void publishingMessagesInBatches() throws IOException, TimeoutException, InterruptedException {
        try (Connection connection = createConnection()) {
            // 1. 开启信道
            Channel channel = connection.createChannel();
            // 2. 设置信道为 confirm  模式
            channel.confirmSelect();
            // 3. 声明交换机和队列
            channel.queueDeclare(Connect.PUBLISHER_CONFIRMS_QUEUE2, true, false, false, null);
            // 4. 发送消息，并等待确认
            long start = System.currentTimeMillis();
            // 设置批量窗口大小、当前消息条数
            int batchSize = 200;
            int outstandingMessageCount = 0;
            for (int i = 0; i < MESSAGE_COUNT; i++) {
                String msg = "hello publisher confirms" + i;
                channel.basicPublish("", Connect.PUBLISHER_CONFIRMS_QUEUE2, null, msg.getBytes());
                outstandingMessageCount++;
                if (outstandingMessageCount == batchSize) {
                    channel.waitForConfirmsOrDie(5000);
                    outstandingMessageCount = 0;
                }
            }
            // 处理掉最后剩余不足 outstandingMessageCount 条
            if (outstandingMessageCount > 0) {
                channel.waitForConfirmsOrDie(5000);
            }
            long end = System.currentTimeMillis();
            System.out.printf("批量确认策略, 消息条数: %d, 耗时: %d ms \n", MESSAGE_COUNT, end - start);
        }
    }

    /**
     * 单独确认
     * 单独确认策略, 消息条数: 1000, 耗时: 13852 ms
     */
    private static void publishingMessagesIndividually() throws IOException, TimeoutException, InterruptedException {
        try (Connection connection = createConnection()) {
            // 1. 开启信道
            Channel channel = connection.createChannel();
            // 2. 设置信道为 confirm  模式
            channel.confirmSelect();
            // 3. 声明交换机和队列
            channel.queueDeclare(Connect.PUBLISHER_CONFIRMS_QUEUE1, true, false, false, null);
            // 4. 发送消息，并等待确认
            long start = System.currentTimeMillis();
            for (int i = 0; i < MESSAGE_COUNT; i++) {
                String msg = "hello publisher confirms" + i;
                channel.basicPublish("", Connect.PUBLISHER_CONFIRMS_QUEUE1, null, msg.getBytes());
                // 等待确认
                channel.waitForConfirmsOrDie(5000);
            }
            long end = System.currentTimeMillis();
            System.out.printf("单独确认策略, 消息条数: %d, 耗时: %d ms \n", MESSAGE_COUNT, end - start);
        }
    }
}
