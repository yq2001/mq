package com.kyle.fanout;

import com.kyle.config.RabbitProducerConfig;
import com.kyle.constants.Connect;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import jakarta.annotation.Resource;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: m'y'q
 * \* Date: 2024/9/17 9:23
 * \* Description:
 * \
 */
public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(Connect.HOST);
        connectionFactory.setPort(Connect.PORT);
        connectionFactory.setUsername(Connect.USER_NAME);
        connectionFactory.setPassword(Connect.PASSWORD);
        connectionFactory.setVirtualHost(Connect.VIRTUAL_HOST);
        Connection connection = connectionFactory.newConnection();

        Channel channel = connection.createChannel();

        channel.exchangeDeclare(Connect.FANOUT_EXCHANGE, BuiltinExchangeType.FANOUT, true);

        channel.queueDeclare(Connect.FANOUT_QUEUE1, true, false, false, null);
        channel.queueDeclare(Connect.FANOUT_QUEUE2, true, false, false, null);

        channel.queueBind(Connect.FANOUT_QUEUE1, Connect.FANOUT_EXCHANGE, "");
        channel.queueBind(Connect.FANOUT_QUEUE2, Connect.FANOUT_EXCHANGE, "");

        String msg = "发布订阅：发送消息";
        channel.basicPublish(Connect.FANOUT_EXCHANGE, "", null, msg.getBytes());
        System.out.println("发送消息成功");

        channel.close();
        connection.close();
    }
}
