package com.kyle.config;

import com.kyle.constants.Connect;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: m'y'q
 * \* Date: 2024/9/17 9:14
 * \* Description:
 * \
 */
public class RabbitProducerConfig {

    public static void connectConfig(ConnectionFactory connectionFactory) {
        connectionFactory.setHost(Connect.HOST);
        connectionFactory.setPort(Connect.PORT);
        connectionFactory.setUsername(Connect.USER_NAME);
        connectionFactory.setPassword(Connect.PASSWORD);
        connectionFactory.setVirtualHost(Connect.VIRTUAL_HOST);
    }

}
